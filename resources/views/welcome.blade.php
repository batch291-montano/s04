@extends('layouts.app')

@section('content')

    <div>
        <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" width = "350" height ="450" class ="d-block mx-auto">
        </div>
    </div>

    <div class="my-2 mx-2">
            <h2 class ="text-center my-3 mx-3">Featured Posts:</h2> 
    </div>

    <div class="my-2 mx-2">
        @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                        <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                        <h6 class="card-text mb-3">Active: {{$post->isActive}}</h6>

                        <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                    </div>
                </div>
            @endforeach
            @else  
            <div>
                <h2>There are no posts to show</h2>  
                <a href="/posts/create" class="btn btn-info">Create post</a>
            </div> 
        @endif
@endsection