@extends('layouts.app')

@section('content')

    <form method="POST" action="/posts/{{ $post->id }}" >   
        @method('PUT') 
        @csrf
        <div class="form-group my-2 mx-2">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}">
        </div>
        <div class="form-group my-2 mx-2">
            <label for="content">Content</label>
            <textarea class="form-control" id="content" name="content">{{ $post->content }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
